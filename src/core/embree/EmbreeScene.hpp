#ifndef CROSSLIGHT_CORE_EMBREE_EMBREESCENE_H_
#define CROSSLIGHT_CORE_EMBREE_EMBREESCENE_H_


#include "../Scene.hpp"
#include <embree3/rtcore.h>


namespace cl {
    class EmbreeIntersector;


    class EmbreeScene final : public Scene {
    public:
        ~EmbreeScene() override;

        RTCScene getEmbreeScene() const;

    private:
        explicit EmbreeScene(const std::shared_ptr<EmbreeIntersector> &intersector);

        void doAddGeometry(
            const std::shared_ptr<TriangleMesh> &mesh,
            const Transform3f &transformCurrentlyIgnored) override;

//        void doRemove(int32_t geometryId) override;

        void doCommit() override;

        RTCScene embScene;

        friend class EmbreeIntersector;
    };
}

#endif  // CROSSLIGHT_CORE_EMBREE_EMBREESCENE_H_

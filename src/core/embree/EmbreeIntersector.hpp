#ifndef CROSSLIGHT_CORE_EMBREE_EMBREEINTERSECTOR_H_
#define CROSSLIGHT_CORE_EMBREE_EMBREEINTERSECTOR_H_


#include "../Intersector.hpp"
#include <embree3/rtcore.h>


namespace cl {
    class EmbreeIntersector final : public Intersector {
    public:
        EmbreeIntersector(const std::string &embreeConfig = "");
        ~EmbreeIntersector() override;

        std::shared_ptr<Scene> createScene(const std::shared_ptr<Intersector> &thisShared) override;

        int getPreferredBatchSize() override;

        std::shared_ptr<RayBatch> createBatch(const std::shared_ptr<Intersector> &thisShared,
                                           int size) override;

        RTCDevice &getEmbreeDevice();

    protected:

        void doIntersectClosest(
            const Scene &scene,
            RayBatch &batch,
            int upTo) override;

        void doIntersectAny(
            const Scene &scene,
            RayBatch &batch,
            int upTo) override;

    private:
        RTCDevice embDevice;

    };

}

#endif  // CROSSLIGHT_CORE_EMBREEMBREEE_INTERSECTOR_H_

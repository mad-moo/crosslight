#include "Intersector.hpp"
#include "RayBatch.hpp"
#include <fancy-assert>


namespace cl {
    void Intersector::intersectClosest(const Scene &scene, RayBatch &batch, int upTo) {
        assertEq(scene.getIntersector().get(), this);
        assert(scene.isCommitted);
        assertLe(upTo, batch.size());
        
        doIntersectClosest(scene, batch, upTo);
    }


    void Intersector::intersectAny(const Scene &scene, RayBatch &batch, int upTo) {
        assertEq(scene.getIntersector().get(), this);
        assert(scene.isCommitted);
        assertLe(upTo, batch.size());

        doIntersectAny(scene, batch, upTo);
    }

}

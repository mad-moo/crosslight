#ifndef CROSSLIGHT_CORE_SCENE_H_
#define CROSSLIGHT_CORE_SCENE_H_


#include <memory>
#include "Transform3f.hpp"
#include "TriangleMesh.hpp"


namespace cl {
    class Intersector;

    class Scene {
    public:
        virtual ~Scene() = default;

        std::shared_ptr<Intersector> getIntersector() const;

        void addGeometry(
            const std::shared_ptr<TriangleMesh> &mesh,
            const Transform3f &transformCurrentlyIgnored);

        void commit();

    protected:
        explicit Scene(const std::shared_ptr<Intersector> &intersector);

        virtual void doAddGeometry(
            const std::shared_ptr<TriangleMesh> &mesh,
            const Transform3f &transformCurrentlyIgnored) = 0;

        virtual void doCommit() = 0;

    private:
        bool isCommitted;
        const std::shared_ptr<Intersector> intersector;

        friend class Intersector;
    };
}

#endif  // CROSSLIGHT_CORE_SCENE_H_

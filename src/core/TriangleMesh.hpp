#ifndef CROSSLIGHT_RENDER_TRIANGLEMESH_H_
#define CROSSLIGHT_RENDER_TRIANGLEMESH_H_


#include <Eigen/Dense>
#include <memory>
#include "SBuffer.hpp"


using namespace Eigen;


namespace cl {

    class TriangleMesh final {
        public:
            TriangleMesh(const SBufferX3f &positions,
                         const SBufferX3i32 &faces);

            ~TriangleMesh() = default;

            int nVertices() const;
            int nFaces() const;

            const SBufferX3f &getPositions() const;
            const SBufferX3i32 &getFaces() const;

        private:
            const SBufferX3f positions;
            const SBufferX3i32 faces;
    };

}

#endif  // CROSSLIGHT_RENDER_TRIANGLEMESH_H_


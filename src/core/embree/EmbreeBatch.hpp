#ifndef CROSSLIGHT_CORE_EMBREE_EMBREEBATCH_H_
#define CROSSLIGHT_CORE_EMBREE_EMBREEBATCH_H_


#include <vector>
#include <embree3/rtcore.h>
#include "../RayBatch.hpp"
#include "EmbreeIntersector.hpp"


using namespace Eigen;


namespace cl {

    class EmbreeBatch final : public RayBatch {
        public:
            ~EmbreeBatch() override = default;

            SBuffer<RTCRayHit, DYN> getEmbreeRayHits();

        private:
            EmbreeBatch(std::shared_ptr<EmbreeIntersector> intersector,
                        int size);

            void clear();

            bool cleared;
            SBuffer<RTCRayHit, DYN> embRayHits;

            friend class EmbreeIntersector;
    };
}

#endif  // CROSSLIGHT_CORE_EMBREE_EMBREEBATCH_H_

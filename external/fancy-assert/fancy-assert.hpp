// Copyright 2016-2018 Jakob Pinterits
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


// Fancy Assertions Library
//
// Fancy Assertions is a header-only library that provides assertions with much
// more descriptive error messages than standard C does.
//
// You can also provide a custom assertion handler to be called whenever
// assertions fail. The default throws a C++ exception so you can easily catch
// problems with your debugger


#ifndef FANCYASSERT_ASSERT_H
#define FANCYASSERT_ASSERT_H


#include <iostream>
#include <sstream>
#include <cstdlib>
#include <typeinfo>


#undef assert


/**
 * \brief The action to take when an assertion fails
 *
 * The macro is passed the assertion's error message.
 *
 * The default writes the error to stderr and throws an AssertionException with
 * the message.
 */
#ifndef FANCYASSERT_ASSERTION_FAILURE_ACTION

#define FANCYASSERT_ASSERTION_FAILURE_ACTION(errorMessage)                     \
    {                                                                          \
        std::cerr << (errorMessage) << std::endl;                              \
        throw ::fanas::AssertionException(errorMessage);                       \
    }

#endif  // FANCYASSERT_ASSERTION_FAILURE_ACTION


namespace fanas {

    class AssertionException final : public std::runtime_error {
    public:
        AssertionException(const char *message)
                : std::runtime_error(message) {}
    };

}

// Internal helper code
namespace fanas_internal {
    inline void serializeLocation(std::ostream &os,
                                  const char *file,
                                  int line) {

        os << file << ":" << line << ": ";
    }

    inline void serializeLocation(std::ostream &os,
                                  const char *file,
                                  int line,
                                  const char *function) {

        serializeLocation(os, file, line);
        os << " in function \'" << function << "\': ";
    }

    class assert_cast_helper final {
    private:
        const char *file;
        int line;
        const char *function;

    public:
        assert_cast_helper(const char *file, int line, const char *function)
                : file(file), line(line), function(function) {}

        template <typename OUT, typename IN>
        OUT perform_cast(IN *input) {
            // casting a null-pointer is legal, but yields null again so the
            // check would fail
            if (input == nullptr)
                return nullptr;

            if (dynamic_cast<OUT>(input) == nullptr) {
                std::stringstream msg;
                fanas_internal::serializeLocation(msg, file, line, function);

                msg << "attempted to cast an instance of \"";
                msg << typeid(*input).name() << "\" to \"";
                msg << typeid(typename std::remove_pointer<OUT>::type).name();
                msg << "\"\n(type names may be name mangled)";

                FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());         
            }

            return static_cast<OUT>(input);
        }

        template <typename OUT, typename IN>
        OUT perform_cast(IN &input) {
            if (dynamic_cast<typename std::remove_reference<OUT>::type*>       \
                    (&input) == nullptr) {

                std::stringstream msg;
                fanas_internal::serializeLocation(msg, file, line, function);
                msg << "attempted to cast an instance of \"";
                msg << typeid(input).name() << "\" to \"";
                msg << typeid(OUT).name() << "\"\n";
                msg << "(type names may be name mangled)";
                FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());
            }

            return static_cast<OUT>(input);
        }
    };
}


#if !defined(NDEBUG) && !defined(FANCYASSERT_NO_ASSERT)


/**
 * @brief true if assertions are enabled, false otherwise
 */
#define FANCYASSERT_ASSERTIONS_ENABLED true

/**
 * @brief Debugging macro which ensures that the given condition is true.
 *
 * This macro does nothing in case the condition is fulfilled. Otherwise an
 * error message is shown and the program terminated.
 *
 * The macro can be turned off by defining 'NDEBUG' before including this
 * header. Doing so will keep the condition from being evaluated, dropping any
 * side effects.
 */
#define assert(condition)                                                      \
    do {                                                                       \
        if(!(condition))                                                       \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #condition "\" failed";                      \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)

/**
 * @brief equivalent to assert(lhs == rhs), but prints a more helpful error
 * message
 */
#define assertEq(lhs, rhs)                                                     \
    do {                                                                       \
        auto lhsv = (lhs);                                                     \
        auto rhsv = (rhs);                                                     \
        if(lhsv != rhsv)                                                       \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #lhs " == " #rhs "\" failed ";               \
            msg << "(" << lhsv << " vs " << rhsv << ")";                       \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)

/**
 * @brief equivalent to assert(lhs != rhs), but prints a more helpful error
 * message
 */
#define assertNeq(lhs, rhs)                                                    \
    do {                                                                       \
        auto lhsv = (lhs);                                                     \
        auto rhsv = (rhs);                                                     \
        if(lhsv == rhsv)                                                       \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #lhs " != " #rhs "\" failed ";               \
            msg << "(" << lhsv << " vs " << rhsv << ")";                       \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)

/**
 * @brief equivalent to assert(lhs < rhs), but prints a more helpful error
 * message
 */
#define assertLt(lhs, rhs)                                                     \
    do {                                                                       \
        auto lhsv = (lhs);                                                     \
        auto rhsv = (rhs);                                                     \
        if(lhsv >= rhsv)                                                       \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #lhs " < " #rhs "\" failed ";                \
            msg << "(" << lhsv << " vs " << rhsv << ")";                       \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)

/**
 * @brief equivalent to assert(lhs <= rhs), but prints a more helpful error
 * message
 */
#define assertLe(lhs, rhs)                                                     \
    do {                                                                       \
        auto lhsv = (lhs);                                                     \
        auto rhsv = (rhs);                                                     \
        if(lhsv > rhsv)                                                        \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #lhs " <= " #rhs "\" failed ";               \
            msg << "(" << lhsv << " vs " << rhsv << ")";                       \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)

/**
 * @brief equivalent to assert(lhs > rhs), but prints a more helpful error
 * message
 */
#define assertGt(lhs, rhs)                                                     \
    do {                                                                       \
        auto lhsv = (lhs);                                                     \
        auto rhsv = (rhs);                                                     \
        if(lhsv <= rhsv)                                                       \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #lhs " > " #rhs "\" failed ";                \
            msg << "(" << lhsv << " vs " << rhsv << ")";                       \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)

/**
 * @brief equivalent to assert(lhs >= rhs), but prints a more helpful error
 * message
 */
#define assertGe(lhs, rhs)                                                     \
    do {                                                                       \
        auto lhsv = (lhs);                                                     \
        auto rhsv = (rhs);                                                     \
        if(lhsv < rhsv)                                                        \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #lhs " >= " #rhs "\" failed ";               \
            msg << "(" << lhsv << " vs " << rhsv << ")";                       \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)


/**
 * @brief an assertion which always fails
 *
 * Can be used to assert that a section of code is never executed.
 */
#define assertNotReached()                                                     \
    do {                                                                       \
        std::stringstream msg;                                                 \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
        msg << "assertion failed: code should not be reached";                 \
        FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());               \
    } while (false)


/**
 * @brief equivalent to assert(typeid(lhs) == typeid(rhs)), but prints a more
 * helpful error message
 */
#define assertTypeEq(lhs, rhs)                                                 \
    do {                                                                       \
        if(typeid(lhs) != typeid(rhs))                                         \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"typeid(" #lhs ") == typeid(" #rhs ")\"";       \
            msg << " failed (" << typeid(lhs).name() << " vs ";                \
            msg << typeid(rhs).name() << ")";                                  \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)


/**
 * @brief equivalent to assert(lhs != nullptr), but prints a more helpful error
 * message
 */
#define assertNull(value)                                                      \
    do {                                                                       \
        auto valuev = (value);                                                 \
        if(valuev != nullptr)                                                  \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #value << " == nullptr\" failed ";           \
            msg << "(the value was " << valuev << ")";                         \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)

/**
 * @brief equivalent to assert(lhs != nullptr), but prints a more helpful error
 * message
 */
#define assertNotNull(value)                                                   \
    do {                                                                       \
        if((value) == nullptr)                                                 \
        {                                                                      \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "assertion \"" #value << " != nullptr\" failed";            \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)

/**
 * @brief succeeds if and only if an expression does not throw an exception
 *
 * <value>'s result is ignored.
 */
#define assertNoThrow(value)                                                   \
    do {                                                                       \
        try {                                                                  \
            (value);                                                           \
        } catch (const std::runtime_error &err) {                              \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "no-throw exception failed: \"" #value;                     \
            msg << "\" has thrown the following exception:\n";                 \
            msg << err.what();                                                 \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        } catch (...) {                                                        \
            std::stringstream msg;                                             \
            fanas_internal::serializeLocation(                                 \
                msg, __FILE__, __LINE__, __func__);                            \
            msg << "no-throw exception failed: \"" #value;                     \
            msg << << "\" has thrown an unrecognized exception";               \
            FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());           \
        }                                                                      \
    } while (false)


/**
 * @brief Casts <value> to <type>, asserting that the cast is legal
 *
 * Uses static_cast to cast <input> to <OUT>. Additionally, if assertions are
 * enabled, uses dynamic_cast to ensure the cast is legal.
 */

// Implementation notes:
// The cast is tricky to implement, because
//
//     - it should resemble C++ style casts, i.e. `static_cast<type>(value)` and
//       thus has to be a template
//     - it needs file/line/function information to print useful information
//
// Thus the macro instantiates a class, passing in the location information and
// then calls a template function on that instance. This gives the function
// access to all required values.
#define assert_cast fanas_internal::assert_cast_helper \
    (__FILE__, __LINE__, __func__).perform_cast


#else  // NDEBUG


#define FANCYASSERT_ASSERTIONS_ENABLED false

#define assert(condition)         0

#define assertEq(lhs, rhs)        0
#define assertNeq(lhs, rhs)       0
#define assertLt(lhs, rhs)        0
#define assertLe(lhs, rhs)        0
#define assertGt(lhs, rhs)        0
#define assertGe(lhs, rhs)        0


#if defined( FANCYASSERT_COMPILER_GCC) || defined(FANCYASSERT_COMPILER_CLANG)
#define assertNotReached()        __builtin_unreachable()
#else
#define assertNotReached()        0
#endif

#define assertTypeEq(lhs, rhs)    0

#define assertNull(value)         0
#define assertNotNull(value)      0

#define assertNoThrow(value)      0

#define assert_cast               static_cast


#endif  // NDEBUG


/**
 * @brief An assertion which always fails with an error message indicating
 * missing functionality
 *
 * @attention Unlike other assertions this one cannot be disabled and will thus
 *            always fail, regardless of whether or not assertions are enabled.
 */
#define TODO()                                                                 \
    do {                                                                       \
        std::stringstream msg;                                                 \
        fanas_internal::serializeLocation(msg, __FILE__, __LINE__);            \
        msg << "Function '" << __func__;                                       \
        msg << "' has unimplemented functionality!" << std::endl;              \
        FANCYASSERT_ASSERTION_FAILURE_ACTION(msg.str().c_str());               \
    } while (false)


#endif  // FANCYASSERT_ASSERT_H

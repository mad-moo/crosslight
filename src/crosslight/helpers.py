import numpy as np
from . import crosslight_swig as clsw


_npy_types = (
    np.bool_, np.bool8,
    np.byte, np.short, np.intc, np.int_, np.longlong, np.intp, np.int8, np.int16, np.int32, np.int64,
    np.ubyte, np.ushort, np.uintc, np.uint, np.ulonglong, np.uintp, np.uint8, np.uint16, np.uint32, np.uint64,
    np.half, np.single, np.double, np.float_, np.longfloat, np.float16, np.float32, np.float64, np.float128, np.csingle,
    np.complex_, np.clongfloat, np.complex64, np.complex128, np.complex256,
    np.object_
)


_npy_type_map = {
    np.dtype(t).num: t for t in _npy_types
}


def numpy_type_from_num(type_num: int):
    """ Converts a numpy type number (as returned by dtype.num) to a dtype object """
    try:
        return _npy_type_map[type_num]
    except KeyError:
        raise ValueError(type_num)


def arg_as_buffer(arg,
                  dtype = None,
                  nvectors: int = None,
                  ndims: int = None):

    def raise_error():
        # create a human readable descriptor for the required argument
        if dtype is None and nvectors is None and ndims:
            type_str = 'array'
        else:
            type_str = 'array of'

            if nvectors is not None:
                type_str += f' {nvectors}'

            if ndims is not None:
                type_str += f' {ndims}D'

            if dtype is not None:
                type_str += f' {np.dtype(dtype).name}(s)'

        raise TypeError(f'Cannot use {type(arg).__name__} as {type_str}')

    # Convert to a buffer
    if isinstance(arg, clsw.Buffer):
        as_buffer = arg

    else:
        # try converting the argument to a numpy array
        try:
            as_array = np.array(arg)
        except TypeError:
            raise_error()

        # then to a buffer
        as_buffer = clsw._buffer_from_numpy_copy_mem(as_array)

    # Ensure the buffer fits the requirements
    if dtype is not None and as_buffer.dtype != dtype:
        raise_error()

    if nvectors is not None and as_buffer.shape[0] != nvectors:
        raise_error()

    if ndims is not None and as_buffer.shape[1] != ndims:
        raise_error()

    return as_buffer


def arg_as_transform(arg):
    # The argument is already a transform
    if isinstance(arg, clsw.Transform3f):
        return arg

    # Try converting the argument to a numpy array
    try:
        as_array = np.array(arg)
    except TypeError:
        raise TypeError(f'Cannot use {type(arg).__name__} as Transform3f')

    # Ensure the array fits the requirements
    if as_array.shape != (3, 4):
        raise TypeError(f'In order to use a numpy array as Transform3f it has to be of shape (3, 4)')

    return clsw.Transform3f(as_array)


# If this is set to `True` functions will perform more in-depth verification of parameters. This can lower performance
# but can be useful when debugging.
_strict_verification = False


def set_strict_verification(val: bool):
    global _strict_verification
    _strict_verification = val


def strict_verification_enabled():
    return _strict_verification


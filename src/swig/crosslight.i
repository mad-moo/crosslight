%module crosslight_swig


%include <std_shared_ptr.i>
%include <std_string.i>
%include <attribute.i>

// Include the necessary C++ headers
%{
    #include "../core/embree/EmbreeIntersector.hpp"

    #include "../core/Buffer.hpp"
    #include "../core/Intersector.hpp"
    #include "../core/RayBatch.hpp"
    #include "../core/Transform3f.hpp"
    #include "../core/type_helpers.hpp"

    // fancy-assert doesn't play nice with python's libraries
    #undef assert
    #include <assert.h>

    #include <sstream>
    #include <Eigen/Dense>

    // Disable the deprecated numpy API
    #define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
    #include <numpy/arrayobject.h>

    using namespace Eigen;
    using namespace cl;
%}


%init %{
    // initialize numpy
    import_array();

    // initialize custom python types
    PyMemoryKeeperType.tp_new = PyType_GenericNew;
    if (PyType_Ready(&PyMemoryKeeperType) < 0)
        return nullptr;
%}


// python imports
%pythoncode %{
    import numpy as np
    from . import helpers
%}


%define MATRIX_TYPEMAP(DTYPE, NPY_DTYPE, NROWS, NCOLS)
%typemap(in) Matrix<DTYPE, NROWS, NCOLS> {
    const bool isVector = NCOLS == 1;

    // Try converting the input to a numpy array
    PyObject *uncastArray = PyArray_FROMANY(
            $input,
            NPY_DTYPE,
            0,
            0,
            NPY_ARRAY_CARRAY);

    // Valid array
    if (uncastArray == nullptr) {
        TODO();
    }

    assert(PyArray_Check(uncastArray));
    PyArrayObject *in = reinterpret_cast<PyArrayObject *>(uncastArray);

    // Valid dimensions
    bool dimsAreValid;
    if (isVector)
        dimsAreValid = PyArray_NDIM(in) == 1 && PyArray_DIM(in, 0) == NROWS;
    else
        dimsAreValid = PyArray_NDIM(in) == 2 && PyArray_DIM(in, 0) == NROWS && PyArray_DIM(in, 1) == NCOLS;

    if (!dimsAreValid) {
        Py_DECREF(uncastArray);
        TODO();
    }

    // copy over the values
    for (int yy=0; yy<NROWS; ++yy) {
        for (int xx=0; xx<NCOLS; ++xx) {
            float &curSrc = *reinterpret_cast<float *>(PyArray_GETPTR2(in, yy, xx));
            float &curDst = $1(yy, xx);
            curDst = curSrc;
        }
    }

    // cleanup
    Py_DECREF(uncastArray);
}

%typemap(out) Matrix<DTYPE, NROWS, NCOLS> {
    // create the array
    npy_intp shape[] = {NROWS, NCOLS};
    $result = PyArray_SimpleNew(
        (NCOLS == 1) ? 1 : 2,
        shape,
        NPY_DTYPE);

    PyArrayObject *castResult = reinterpret_cast<PyArrayObject *>($result);

    // copy over the values
    for (int yy=0; yy<NROWS; ++yy) {
        for (int xx=0; xx<NCOLS; ++xx) {
            float &curSrc = $1(yy, xx);
            float &curDst = *reinterpret_cast<float *>(PyArray_GETPTR2(castResult, yy, xx));
            curDst = curSrc;
        }
    }
}


%enddef


%define EXPOSE_RAYBATCH_BUFFER(NPY_DTYPE, DIMS, FUNC_NAME, GETTER)

PyObject *FUNC_NAME() {
    auto &staticBuffer = $self->GETTER();

    auto dynBuffer = cl::Buffer::createNew(
        staticBuffer.memory(),
        staticBuffer.first(),
        NPY_DTYPE,
        DIMS,
        staticBuffer.shape(),
        staticBuffer.strides()
    );

    return (PyObject*)_numpy_from_buffer_share_mem(dynBuffer);
}

%enddef


// typemaps
MATRIX_TYPEMAP(float, NPY_FLOAT, 3, 1)

MATRIX_TYPEMAP(float, NPY_FLOAT, 3, 4)


%typemap(out) PyArrayObject * {
    $result = reinterpret_cast<PyObject *>($1);
}


// SWIG Instructions
%shared_ptr(cl::Buffer)
%shared_ptr(cl::Intersector)
%shared_ptr(cl::RayBatch)
%shared_ptr(cl::TriangleMesh)
%shared_ptr(cl::Scene)


// Attributes
%attribute(cl::Transform3f, Vector3f, translation, getTranslation, setTranslation);
%attribute(cl::Transform3f, Vector3f, x_axis, getXAxis, setXAxis);
%attribute(cl::Transform3f, Vector3f, y_axis, getYAxis, setYAxis);
%attribute(cl::Transform3f, Vector3f, z_axis, getZAxis, setZAxis);


// Typedefs
// Inform SWIG that certain names are interchangable
typedef Matrix<float, 3, 1> Vector3f;


// Helpers
%{
    bool numpyDataTypeIsSupported(NPY_TYPES dType) {
        switch (dType) {
            case NPY_INT8:
            case NPY_INT16:
            case NPY_INT32:
            case NPY_INT64:
            case NPY_UINT8:
            case NPY_UINT16:
            case NPY_UINT32:
            case NPY_UINT64:
            // case NPY_FLOAT16:
            case NPY_FLOAT32:
            case NPY_FLOAT64:
                return true;

            default:
                return false;
        }
    }

    struct PyMemoryKeeperObject {
        PyObject_HEAD
        std::shared_ptr<MemoryKeeper> memory;
    };

    static void pyMemoryKeeperObject_dealloc(PyMemoryKeeperObject *self) {
        self->memory.reset();
        Py_TYPE(self)->tp_free((PyObject*)self);
    }

    static PyTypeObject PyMemoryKeeperType = {
        PyVarObject_HEAD_INIT(NULL, 0)
        "PyMemoryKeeper",                           // tp_name
        sizeof(PyMemoryKeeperObject),               // tp_basicsize
        0,                                          // tp_itemsize
        (destructor)pyMemoryKeeperObject_dealloc,   // tp_dealloc
        0,                                          // tp_print
        0,                                          // tp_getattr
        0,                                          // tp_setattr
        0,                                          // tp_reserved
        0,                                          // tp_repr
        0,                                          // tp_as_number
        0,                                          // tp_as_sequence
        0,                                          // tp_as_mapping
        0,                                          // tp_hash
        0,                                          // tp_call
        0,                                          // tp_str
        0,                                          // tp_getattro
        0,                                          // tp_setattro
        0,                                          // tp_as_buffer
        Py_TPFLAGS_DEFAULT,                         // tp_flags
        "Python Wrapper for MemoryKeeper",          // tp_doc
    };


    /**
     * Creates a numpy NdArray with the same contents as this buffer.
     *
     * The **memory is shared** and continues to be owned by `this`
     * buffer. The numpy array is set up to reference-count the memory
     * as well. It is thus safe to destroy `this` instance even if the
     * numpy array is still valid.
     */
    PyArrayObject *_numpy_from_buffer_share_mem(
        const std::shared_ptr<cl::Buffer> &buffer) {

        // convert the shape to npy_intp, as required by numpy
        const int *shape = buffer->shape();
        npy_intp shapeIntPtr[MAX_BUFFER_ORDER];
        std::copy(&shape[0], &shape[buffer->order()], &shapeIntPtr[0]);

        // convert the strides to npy_intp, as required by numpy
        const int *strides = buffer->strides();
        npy_intp stridesIntPtr[MAX_BUFFER_ORDER];
        std::copy(&strides[0], &strides[buffer->order()], &stridesIntPtr[0]);

        PyArray_Descr *descr = PyArray_DescrFromType(buffer->dType());

        PyObject *rawResult = PyArray_NewFromDescr(
            &PyArray_Type,
            descr,
            buffer->order(),
            shapeIntPtr,
            stridesIntPtr,
            buffer->first(),
            NPY_ARRAY_WRITEABLE,
            NULL);

        assert(PyArray_Check(rawResult));
        auto *result = reinterpret_cast<PyArrayObject *>(rawResult);

        // Set up memory management
        //
        // Numpy arrays can point to a "base object". This object is
        // reference counted and thus destroyed when no longer needed.
        //
        // The idea is to create a new python type which drops the
        // reference to the buffer's memory object when it dies.
        PyMemoryKeeperObject *baseObj = PyObject_New(
            PyMemoryKeeperObject,
            &PyMemoryKeeperType);

        assertNotNull(baseObj);

        // Initialize the python object's fields. Because the struct
        // was created by C (rather than C++) no constructor has been
        // called and all fields are uninitialized.
        new(&baseObj->memory) std::shared_ptr<MemoryKeeper>(
            buffer->memory());

        PyArray_SetBaseObject(
            result,
            reinterpret_cast<PyObject *>(baseObj));

        return result;
    }

%}


namespace cl {

    class Transform3f {
    public:
        ~Transform3f() = default;

        %extend {
            /**
            * The Transform constructor uses a reference to the transform. This
            * causes a problem with the typemap because SWIG initializes the result
            * to `nullptr`. This could probably be overcome with more knowledge
            * about SWIG.
            */
            Transform3f(Matrix<float, 3, 4> transform) {
                return new Transform3f(transform);
            }

            /**
             * These methods use C++ camel case naming convention, but should be
             * exposed to python in lowercase.
             */
            static Transform3f identity() {
                return Transform3f::Identity();
            }

            std::string __repr__() {
                std::stringstream os;
                Matrix<float, 3, 4> m = $self->asMatrix();

                os << "Transform3f([[" << m(0, 0) << ", " << m(0, 1) << ", " << m(0, 2) << ", " << m(0, 3) << "],\n";
                os << "             [" << m(1, 0) << ", " << m(1, 1) << ", " << m(1, 2) << ", " << m(1, 3) << "],\n";
                os << "             [" << m(2, 0) << ", " << m(2, 1) << ", " << m(2, 2) << ", " << m(2, 3) << "]])";

                return os.str();
            }
        };
    };


    class Buffer {
    public:
        Buffer() = delete;
        ~Buffer() = default;

        %extend {
            int __len__() {
                return $self->shape(0);
            }

            // --- HELPER FUNCTIONS ---
            // These functions are not publicly exposed in the python interface. They provide functionality to python
            // functions which provide a prettier interface
            int _get_dtype_num() {
                return $self->dType();
            }

            int _get_size(int dim) {
                return $self->shape(dim);
            }

            int _get_order() {
                return $self->order();
            }
        };

        %pythoncode %{
            @property
            def shape(self):
                return tuple(self._get_size(ii) for ii in range(self._get_order()))

            @property
            def dtype(self):
                return helpers.numpy_type_from_num( self._get_dtype_num() )

            def __repr__(self):
                strshape = '*'.join(str(x) for x in self.shape)
                return f'<crosslight.Buffer object at 0x{id(self):x} containing {strshape} numpy.{self.dtype.__name__} object(s)>'
        %}
    };


    class Intersector {
    public:
        virtual ~Intersector() = default;

        virtual int getPreferredBatchSize() = 0;

        virtual std::shared_ptr<RayBatch> createBatch(
            const std::shared_ptr<Intersector> &thisShared,
            int size) = 0;

        // These methods take references, but SWIG was told to use shared
        // pointers. They are wrapped here.
        %extend {
            void _intersect_closest(
                const std::shared_ptr<cl::Scene> &scene,
                const std::shared_ptr<cl::RayBatch> &batch,
                int upTo) {

                $self->intersectClosest(*scene, *batch, upTo);
            }

            void _intersect_any(
                const std::shared_ptr<cl::Scene> &scene,
                const std::shared_ptr<cl::RayBatch> &batch,
                int upTo) {

                $self->intersectAny(*scene, *batch, upTo);
            }
        }

    };


    class RayBatch {
    public:
        RayBatch() = delete;
        virtual ~RayBatch() = default;

        %extend {

            // Allow access to the batches' buffers. The returned arrays share
            // memory with the RayBatch, thus giving python read and write
            // access.
            //
            // The buffers are exposed as python properties further below.

            EXPOSE_RAYBATCH_BUFFER(NPY_UINT,    1, _get_ray_ids,       getRayIdBuffer)
            EXPOSE_RAYBATCH_BUFFER(NPY_FLOAT32, 2, _get_origins,       getOriginBuffer)
            EXPOSE_RAYBATCH_BUFFER(NPY_FLOAT32, 2, _get_directions,    getDirBuffer)
            EXPOSE_RAYBATCH_BUFFER(NPY_FLOAT32, 1, _get_lengths,       getLengthBuffer)
            EXPOSE_RAYBATCH_BUFFER(NPY_UINT,    1, _get_geometry_ids,  getGeometryIdBuffer)
            EXPOSE_RAYBATCH_BUFFER(NPY_UINT,    1, _get_primitive_ids, getPrimitiveIdBuffer)

            int __len__() {
                return $self->size();
            }
        }

        %pythoncode %{
            @property
            def ray_ids(self):
                return self._get_ray_ids()

            @property
            def origins(self):
                return self._get_origins()

            @property
            def directions(self):
                return self._get_directions()

            @property
            def lengths(self):
                return self._get_lengths()

            @property
            def geometry_ids(self):
                return self._get_geometry_ids()

            @property
            def primitive_ids(self):
                return self._get_primitive_ids()
        %}
    };

    class TriangleMesh {
        public:
            TriangleMesh() = delete;
            ~TriangleMesh() = default;
    };

    class Scene {
    public:
        Scene() = delete;
        virtual ~Scene() = default;

        void addGeometry(
            const std::shared_ptr<cl::TriangleMesh> &mesh,
            const Transform3f &transformCurrentlyIgnored);

        void commit();
    };
}

%inline %{


    /**
     * Creates a buffer matching the NdArray. The contents are copied
     * over, no memory is being shared.
     */
    std::shared_ptr<cl::Buffer> _buffer_from_numpy_copy_mem(PyObject *obj) {

        // Enforce a standard memory layout
        // TODO does this thing have to be unreffed once done?
        PyObject* pyArrayObj = PyArray_FromAny(
                obj,
                nullptr,
                0,
                0,
                NPY_ARRAY_CARRAY,
                nullptr);

        // TODO what happens when an invalid type was passed? The docs
        // don't mention this.
        assertNotNull(pyArrayObj);
        assert(PyArray_Check(pyArrayObj));
        PyArrayObject *pyArray = reinterpret_cast<PyArrayObject*>(pyArrayObj);

        // Create a buffer matching the input's size
        const int srcOrder = PyArray_NDIM(pyArray);
        const npy_intp *srcShape = PyArray_SHAPE(pyArray);
        const PyArray_Descr &srcTypeDesc = *PyArray_DTYPE(pyArray);
        const NPY_TYPES srcType = static_cast<NPY_TYPES>(
            srcTypeDesc.type_num );
        const cl::type_info srcTypeInfo(srcType);


        // `srcOrder` is zero in inaccessible arrays
        //  https://docs.scipy.org/doc/numpy-1.13.0/reference/c-api.types-and-structures.html#c.PyArrayObject.nd
        if (srcOrder == 0) {
            // FIXME: Setting an error and returning null with shared
            // pointers makes cpython complain.
            PyErr_SetString(
                PyExc_ValueError,
                "The given numpy array is inaccessible. Make sure it uses a numeric type.");

            return nullptr;
        }

        // Enforce the maximum dimensionality supported by buffers
        if (srcOrder > MAX_BUFFER_ORDER) {
            // FIXME: Setting an error and returning null with shared
            // pointers makes cpython complain.
            PyErr_SetString(
                PyExc_ValueError,
                "The given numpy array has more dimensions than are supported");

            return nullptr;
        }

        if (! numpyDataTypeIsSupported(srcType)) {
            // FIXME: Setting an error and returning null with shared
            // pointers makes cpython complain.
            PyErr_SetString(
                PyExc_ValueError,
                "The input array uses an unsupported datatype");

            return nullptr;
        }

        // Create the new Buffer object
        int shapeInt[MAX_BUFFER_ORDER];
        std::copy(&srcShape[0], &srcShape[srcOrder], &shapeInt[0]);
        std::shared_ptr<Buffer> result = Buffer::createNew(
            srcOrder,
            shapeInt,
            srcType);

        // Copy over the contents
        // The numpy array is contiguous because the corresponding flag
        // was passed
        assertEq(PyArray_FLAGS(pyArray) & NPY_ARRAY_C_CONTIGUOUS, NPY_ARRAY_C_CONTIGUOUS);

        // The Buffer is contiguous, because the constructor
        // guarantees it
        {
            const int *shape = result->shape();
            const int *strides = result->strides();

            for (int ii=0; ii<srcOrder-1; ++ii) {
                assertEq(strides[ii], strides[ii+1] * shape[ii + 1]);
            }
        }

        // Thus the data can be memcopied
        size_t nElements = 1;
        for (int ii=0; ii<srcOrder; ++ii) {
            nElements *= srcShape[ii];
        }

        // Copy over the contents
        memcpy(
            result->first(),
            PyArray_DATA(pyArray),
            nElements * srcTypeInfo.size
        );

        return result;
    }


    /**
     * Creates a new intersector. The implementation can be selected using the
     * `name` parameter. If the name doesn't match any backend null/None is
     * returned.
     */
    std::shared_ptr<cl::Intersector> _create_intersector_by_name(std::string name) {
        if (name == "embree") {
            return std::make_shared<EmbreeIntersector>();
        }

        return nullptr;
    }

    /**
     * Creates a fresh RayBatch from an intersector. The intersector's method
     * requires a shared_ptr to the intersector so it cannot be used directly.
     * This method just forwards the call with the correct parameters.
     */
     std::shared_ptr<cl::RayBatch> _create_ray_batch(
            const std::shared_ptr<cl::Intersector> &intersector,
            int batch_size) {

        return intersector->createBatch(intersector, batch_size);
    }

    /**
     * Creates a new Scene from an intersector. The intersector's method
     * requires a shared_ptr to the intersector so it cannot be used directly.
     * This method just forwards the call with the correct parameters.
     */
    std::shared_ptr<cl::Scene> _create_scene(
        const std::shared_ptr<cl::Intersector> &intersector) {

        return intersector->createScene(intersector);
    }

    /**
     * Creates a TriangleMesh. The constructor uses static buffers, but these
     * are not exposed to python. This function handles the conversion.
     */
    std::shared_ptr<cl::TriangleMesh> _create_trianglemesh(
        const std::shared_ptr<cl::Buffer> &positions,
        const std::shared_ptr<cl::Buffer> &faces) {

        SBufferX3f staticPositions(*positions);
        SBufferX3i32 staticFaces(*faces);

        return std::make_shared<cl::TriangleMesh>(
            staticPositions,
            staticFaces
        );
    }
%}

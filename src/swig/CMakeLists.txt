include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_CORE_FLAGS "")

set_property(SOURCE crosslight.i PROPERTY CPLUSPLUS ON)
set_property(SOURCE crosslight.i
        PROPERTY SWIG_FLAGS
        "-includeall"

        # SWIG has trouble finding its files on windows
        #   -> Explicitly set the path
        "-I${SWIG_DIR}/Lib/python"
        "-I${SWIG_DIR}/Lib"
)

swig_add_library(crosslight_swig
    LANGUAGE python
    SOURCES crosslight.i
    OUTPUT_DIR ${CMAKE_BINARY_DIR}/build/crosslight
)

swig_link_libraries(crosslight_swig
    crosslight_core
    ${PYTHON_LIBRARIES}
)

set_target_properties(crosslight_swig
    PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/build/crosslight
)


#ifndef CROSSLIGHT_COMMON_TRANSFORM3F_H_
#define CROSSLIGHT_COMMON_TRANSFORM3F_H_

#include <Eigen/Dense>
#include <math.h>
#include <algorithm>
#include <ostream>
#include <stdint.h>

using namespace Eigen;

namespace cl {

    class Transform3f {
    private:
        Matrix<float, 3, 4> transMat;

    public:
        /**
         * \brief Create an uninitialized instance.
         */
        Transform3f() = default;

        explicit Transform3f(const Matrix<float, 3, 4> &transform)
                : transMat(transform) {
        }

        // Transform3f(const Vector3f &scale,
        //             const Matrix3f &rotation,
        //             const Vector3f &translation) {
        //     transMat.setColumn(0, rotation.col(0) * scale.x());
        //     transMat.setColumn(1, rotation.col(1) * scale.y());
        //     transMat.setColumn(2, rotation.col(2) * scale.z());
        //     transMat.setColumn(3, translation);
        // }

        // Transform3f(const Vector3f &scale,
        //             const quat &rotation,
        //             const Vector3f &translation)
        //         : Transform3f(scale, rotation.asRotationMatrix(), translation) {
        // }

        static Transform3f Identity() {
            Transform3f result;
            result.transMat = Matrix<float, 3, 4>::Identity();
            return result;
        }

        ~Transform3f() = default;

    //     Transform3f inverse() const {
    //         Transform3f result = identity();
    //         result.translate(-getTranslation());
    //         result.transMat = mat34(getRotationMatrix().inverse()) * result.transMat;
    //         result.scale(1.f / getScale());
    //         return std::move(result);
    //     }

    //     void invert() {
    //         *this = inverse();
    //     }


        const Matrix<float, 3, 4> &asMatrix() const {
            return transMat;
        }

        Vector3f getTranslation() const {
            return transMat.col(3);
        }

        void setTranslation(Vector3f value) {
            transMat.col(3) = value;
        }

        Vector3f getScale() const {
            return Vector3f(transMat.col(0).norm(),
                            transMat.col(1).norm(),
                            transMat.col(2).norm());
        }

        Vector3f getXAxis() const {
            return transMat.col(0);
        }

        void setXAxis(Vector3f value) {
            transMat.col(0) = value;
        }

        Vector3f getYAxis() const {
            return transMat.col(1);
        }

        void setYAxis(Vector3f value) {
            transMat.col(1) = value;
        }

        Vector3f getZAxis() const {
            return transMat.col(2);
        }

        void setZAxis(Vector3f value) {
            transMat.col(2) = value;
        }

        Matrix3f getRotationMatrix() const {
            Matrix3f result;
            Vector3f scale = getScale();

            result.col(0) = transMat.col(0) / scale.x();
            result.col(1) = transMat.col(1) / scale.y();
            result.col(2) = transMat.col(2) / scale.z();

            return result;
        }

        void translate(Vector3f amount) {
            setTranslation(getTranslation() + amount);
        }

        void translate(float x, float y, float z) {
            translate(Vector3f(x, y, z));
        }

        void translate(float amount) {
            translate(amount, amount, amount);
        }

        void scale(Vector3f amount) {
            transMat.row(0) = transMat.row(0) * amount.x();
            transMat.row(1) = transMat.row(1) * amount.y();
            transMat.row(2) = transMat.row(2) * amount.z();
        }

        void scale(float x, float y, float z) {
            scale(Vector3f(x, y, z));
        }

        void scale(float amount) {
            scale(amount, amount, amount);
        }

    //     void rotateX(float amount) {
    //         transMat = mat34(Matrix3f::rotate3DX(amount)) * transMat;
    //     }

    //     void rotateY(float amount) {
    //         transMat = mat34(Matrix3f::rotate3DY(amount)) * transMat;
    //     }

    //     void rotateZ(float amount) {
    //         transMat = mat34(Matrix3f::rotate3DZ(amount)) * transMat;
    //     }

    //     void rotate(quat rotation) {
    //         transMat = mat34(rotation.asRotationMatrix()) * transMat;
    //     }

        Transform3f operator*(const Transform3f other) const {
            Matrix4f this4;
            this4 << transMat, 0, 0, 0, 1;

            Matrix4f other4;
            other4 << other.transMat, 0, 0, 0, 1;

            Matrix4f result4 = this4 * other4;
            Matrix<float, 3, 4> result34 = result4.topLeftCorner(3, 4);
            return Transform3f(result34);
        }

        Vector3f apply(const Vector3f &point) const {
            Matrix4f transMat4;
            transMat4 << transMat, 0, 0, 0, 1;
            return (transMat4 * point.homogeneous()).hnormalized().head<3>();
        }

        Vector4f apply(const Vector4f &point) const {
            Matrix4f transMat4;
            transMat4 << transMat, 0, 0, 0, 1;
            return transMat4 * point;
        }
    };
}

#endif  // CROSSLIGHT_COMMON_TRANSFORM3F_H_

#include "EmbreeScene.hpp"
#include "EmbreeIntersector.hpp"
#include <fancy-assert>


namespace cl {

    EmbreeScene::EmbreeScene(const std::shared_ptr<EmbreeIntersector> &intersector)
        : Scene( std::static_pointer_cast<Intersector>(intersector) ),
          embScene(rtcNewScene(intersector->getEmbreeDevice())) {}

    EmbreeScene::~EmbreeScene() {

        rtcReleaseScene(embScene);
    }

    RTCScene EmbreeScene::getEmbreeScene() const {
        return embScene;
    }

    void EmbreeScene::doAddGeometry(
            const std::shared_ptr<TriangleMesh> &mesh,
            const Transform3f &transformCurrentlyIgnored) {

        // get the embree device for this scene
        EmbreeIntersector &intersector = \
            assert_cast<EmbreeIntersector&>(*getIntersector());

        RTCDevice embDevice = intersector.getEmbreeDevice();

        // create a RTCGeometry for the mesh
        RTCGeometry embGeom = rtcNewGeometry(embDevice, RTC_GEOMETRY_TYPE_TRIANGLE);

        // vertices
        float *embVertices = (float *)rtcSetNewGeometryBuffer(
            embGeom,
            RTC_BUFFER_TYPE_VERTEX,
            0,
            RTC_FORMAT_FLOAT3,
            4 * sizeof(float),
            mesh->nVertices());

        const SBufferX3f &meshVertices = mesh->getPositions();

        for (int ii=0; ii<mesh->nVertices(); ++ii) {
             embVertices[4*ii + 0] = meshVertices.get(ii, 0);
             embVertices[4*ii + 1] = meshVertices.get(ii, 1);
             embVertices[4*ii + 2] = meshVertices.get(ii, 2);
        }

        // indices
        uint32_t *embIndices = (uint32_t*)rtcSetNewGeometryBuffer(
            embGeom,
            RTC_BUFFER_TYPE_INDEX,
            0,
            RTC_FORMAT_UINT3,
            4*sizeof(uint32_t),
            mesh->nFaces());

        const SBufferX3i32 &meshFaces = mesh->getFaces();

         for (int ii=0; ii<mesh->nFaces(); ++ii) {
             embIndices[4*ii + 0] = meshFaces.get(ii, 0);
             embIndices[4*ii + 1] = meshFaces.get(ii, 1);
             embIndices[4*ii + 2] = meshFaces.get(ii, 2);
         }

         // finish the geometry
         rtcCommitGeometry(embGeom);

         // add it to the scene
         rtcAttachGeometry(embScene, embGeom);

         // release it
         rtcReleaseGeometry(embGeom);
    }

//    void EmbreeScene::doRemove(int32_t geometryId) {
//        unsigned int embGeomId = geometryId;
//        rtcDetachGeometry(embScene, embGeomId);
//    }

    void EmbreeScene::doCommit() {
        rtcCommitScene(embScene);
    }
}

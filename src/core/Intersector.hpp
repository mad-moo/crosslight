#ifndef CROSSLIGHT_CORE_INTERSECTOR_H_
#define CROSSLIGHT_CORE_INTERSECTOR_H_

#include <memory>
#include "Scene.hpp"

namespace cl {
    class RayBatch;

    class Intersector {
    public:
        virtual ~Intersector() = default;
        
        virtual std::shared_ptr<Scene> createScene(const std::shared_ptr<Intersector> &thisShared) = 0;

        virtual int getPreferredBatchSize() = 0;

        virtual std::shared_ptr<RayBatch> createBatch(const std::shared_ptr<Intersector> &thisShared,
                                                      int size) = 0;

        void intersectClosest(const Scene &scene,
                              RayBatch &batch,
                              int upTo);

        void intersectAny(const Scene &scene,
                          RayBatch &batch,
                          int upTo);


    protected:
        Intersector() = default;

        virtual void doIntersectClosest(
            const Scene &scene,
            RayBatch &batch,
            int upTo) = 0;

        virtual void doIntersectAny(
            const Scene &scene,
            RayBatch &batch,
            int upTo) = 0;

    };

}

#endif  // CROSSLIGHT_CORE_INTERSECTOR_H_

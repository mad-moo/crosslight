import numpy as np
import crosslight as cl
from typing import List


class TriangleMesh(cl.Geometry):
    def __init__(self, positions: np.ndarray, faces: np.ndarray):
        super().__init__()

        if positions.shape[1] != 3:
            raise ValueError(f'The positions of triangle geometrie\'s have to be 3d vectors.'
                             f'The passed in array has {positions.shape[1]}')

        if faces.shape[1] != 3:
            raise ValueError(f'The face indices of triangle geometrie\'s have to be 3d vectors.'
                             f'The passed in array has {faces.shape[1]}')

        self.positions = positions.astype(np.float32)
        self.faces = faces.astype(np.int32)

        # Ensure the indices from the face buffer are not out of range
        if cl.helpers.strict_verification_enabled():
            min_index = np.min(self.faces)
            max_index = np.max(self.faces)

            assert(min_index >= 0)
            assert(max_index < len(self.positions))

    @staticmethod
    def create_box(dimensions: List[float] = 1.0):
        """
        Creates a new TriangleMesh in the shape of a box. One of the boxes'corners is placed at the origin, the other is
        controlled by the `dimensions` parameter.

        `half_sizes` can be used to control the boxes' size. It can either be a single number or a list-like of three
        numbers.
        """

        if isinstance(dimensions, (int, float)):
            sx = sy = sz = dimensions
        elif len(dimensions) == 1:
            sx = sy = sz = dimensions
        elif len(dimensions) == 3:
            sx, sy, sz = dimensions
        else:
            raise ValueError(dimensions)

        vertex_positions = np.array(
            [[0.0, 0.0, 0.0],
             [0.0, 0.0,  sz],
             [0.0,  sy, 0.0],
             [0.0,  sy,  sz],
             [ sx, 0.0, 0.0],
             [ sx, 0.0,  sz],
             [ sx,  sy, 0.0],
             [ sx,  sy,  sz]], dtype=np.float32)

        face_indices = np.array([
            # Back
            [0, 2, 4],
            [2, 6, 4],

            # Front
            [1, 3, 5],
            [3, 7, 5],

            # Left
            [0, 3, 1],
            [0, 2, 3],

            # Right
            [4, 7, 5],
            [4, 6, 7],

            # Bottom
            [0, 5, 4],
            [0, 1, 5],

            # Top
            [2, 7, 6],
            [2, 3, 7]], dtype=np.int32)

        return TriangleMesh(vertex_positions, face_indices)

    @property
    def n_vertices(self):
        return len(self.positions)

    @property
    def n_faces(self):
        return len(self.faces)



#include "Scene.hpp"


namespace cl {

    Scene::Scene(const std::shared_ptr<Intersector> &intersector)
        : isCommitted(false),
          intersector(intersector) {}

    std::shared_ptr<Intersector> Scene::getIntersector() const {
        return intersector;
    }

    void Scene::addGeometry(
        const std::shared_ptr<TriangleMesh> &mesh,
        const Transform3f &transformCurrentlyIgnored) {

        isCommitted = false;
        return doAddGeometry(mesh, transformCurrentlyIgnored);
    }

    void Scene::commit() {
        doCommit();
        isCommitted = true;
    }
}

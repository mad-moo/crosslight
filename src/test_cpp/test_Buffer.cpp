#include <gtest/gtest.h>
#include "../core/Buffer.hpp"


using namespace cl;


template<typename T>
static void testSubSingleSubBuffer2D(
        const std::shared_ptr<Buffer> &parentBuffer,
        int first0, int size0,
        int first1, int size1) {

    const int firstIndex[] = {first0, first1};
    const int subShape[] = {size0, size1};

    auto childBuffer = parentBuffer->subBuffer(
            firstIndex,
            subShape);

//    std::cout << childBuffer << std::endl << std::endl;

    ASSERT_EQ(childBuffer->shape(0), size0);
    ASSERT_EQ(childBuffer->shape(1), size1);

    {
        const int childIndex[] = {0, 0};
        const int parIndex[] = {first0, first1};

        ASSERT_EQ(childBuffer->get<T>(2, childIndex),
                parentBuffer->get<T>(2, parIndex));
    }

    {
        const int childIndex[] = {childBuffer->shape(0) - 1, childBuffer->shape(1) - 1};
        const int parIndex[] = {first0 + size0 - 1, first1 + size1 - 1};

        ASSERT_EQ(childBuffer->get<T>(2, childIndex),
                  parentBuffer->get<T>(2, parIndex));
    }
}


TEST (TestBuffer, testShape) {
    // Instantiate a new buffer and test that the buffer's runtime shape is correct.

    const int SHAPE[] = {1, 2, 3, 4};
    auto buf = Buffer::createNew(4, SHAPE, NPY_UINT8);

    ASSERT_EQ(buf->shape(0), 1);
    ASSERT_EQ(buf->shape(1), 2);
    ASSERT_EQ(buf->shape(2), 3);
    ASSERT_EQ(buf->shape(3), 4);
}


TEST (TestBuffer, testStrides) {
    // Instantiate a new buffer and test that the buffer's runtime strides are correct.

    const int SHAPE[] = {1, 2, 3, 4};
    auto buf = Buffer::createNew(4, SHAPE, NPY_UINT8);

    ASSERT_EQ(buf->stride(0), 24);
    ASSERT_EQ(buf->stride(1), 12);
    ASSERT_EQ(buf->stride(2), 4);
    ASSERT_EQ(buf->stride(3), 1);
}


TEST (TestBuffer, testSimpleSubBuffer) {
    // initialize the buffer
    const int SHAPE[] = {10, 5};
    auto parentBuffer = Buffer::createNew(2, SHAPE, NPY_INT32);

    ASSERT_EQ(parentBuffer->shape(0), SHAPE[0]);
    ASSERT_EQ(parentBuffer->shape(1), SHAPE[1]);

    for (int i0=0; i0<parentBuffer->shape(0); ++i0) {
        for (int i1=0; i1<parentBuffer->shape(1); ++i1) {
            int index[] = {i0, i1};
            parentBuffer->get<int32_t>(2, index) = i0 * 10 + i1;
        }
    }

//    std::cout << "Parent buffer" << std::endl;
//    std::cout << parentBuffer << std::endl << std::endl;

//    std::cout << "Full child buffer" << std::endl;
    testSubSingleSubBuffer2D<int32_t>(
            parentBuffer,
            0, SHAPE[0],
            0, SHAPE[1]);

//    std::cout << "Missing rows" << std::endl;
    testSubSingleSubBuffer2D<int32_t>(
            parentBuffer,
            1, SHAPE[0] - 3,
            0, SHAPE[1]);

//    std::cout << "Missing columns" << std::endl;
    testSubSingleSubBuffer2D<int32_t>(
            parentBuffer,
            0, SHAPE[0],
            1, SHAPE[1] - 3);

//    std::cout << "Missing rows and columns" << std::endl;
    testSubSingleSubBuffer2D<int32_t>(
            parentBuffer,
            1, SHAPE[0] - 3,
            1, SHAPE[1] - 3);

}


TEST (TestBuffer, testBufferAsImage) {
    // Set each pixel in a 3D (height/width/channels) buffer

    const int SHAPE[] = {512, 1024, 3};
    auto buf = Buffer::createNew(3, SHAPE, NPY_INT32);

    // set all pixels to a fixed value
    {
        int index = 0;
        memset(buf->getVoid(1, &index), 1, SHAPE[0] * SHAPE[1] * SHAPE[2] * sizeof(int32_t));
    }

    // use per component indexing to verify the values
    for (int s0=0; s0 < SHAPE[0]; ++s0) {
        for (int s1 = 0; s1 < SHAPE[1]; ++s1) {
            for (int s2 = 0; s2 < SHAPE[2]; ++s2) {

                const int index[] = {s0, s1, s2};
                int &cur = buf->get<int32_t>(3, index);

                ASSERT_EQ(cur, 0x01010101);
            }
        }
    }
}

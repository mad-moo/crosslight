#include "EmbreeBatch.hpp"
#include <fancy-assert>
#include <stddef.h>


namespace cl {
    EmbreeBatch::EmbreeBatch(std::shared_ptr<EmbreeIntersector> intersector, int size)
        : RayBatch(intersector, size),
          embRayHits(SBuffer<RTCRayHit, DYN>::create<1>( Matrix<int, 1, 1>(size)) ) {

        // Initialize all the unused fields.
        // These fields are not changed when tracing so they only have to be set once. Fields
        // that are changed by embree are set in the `clear` call instead.
        for (int ii=0; ii<size; ++ii) {
            RTCRay &ray = embRayHits.get(ii).ray;
            ray.tnear = 0.0001f;
            ray.mask = 0;
            ray.flags = 0;
        }

        // make sure it is safe to cast between embree and eigen datatypes
        // ensure that: eigen doesn't add padding in front of the vector data
        const Vector3f eiVec3;
        assert(eiVec3.data() == reinterpret_cast<const float*>(&eiVec3));

        // ensure that the vector components in embree aren't padded
        static_assert(offsetof(RTCRay, org_y) == offsetof(RTCRay, org_x) + 4, "Unexpected padding in RayBatch");
        static_assert(offsetof(RTCRay, org_z) == offsetof(RTCRay, org_y) + 4, "Unexpected padding in RayBatch");

        static_assert(offsetof(RTCRay, dir_y) == offsetof(RTCRay, dir_x) + 4, "Unexpected padding in RayBatch");
        static_assert(offsetof(RTCRay, dir_z) == offsetof(RTCRay, dir_y) + 4, "Unexpected padding in RayBatch");

        static_assert(offsetof(RTCHit, v) == offsetof(RTCHit, u) + 4, "Unexpected padding in RayBatch");

        static_assert(offsetof(RTCHit, Ng_y) == offsetof(RTCHit, Ng_x) + 4, "Unexpected padding in RayBatch");
        static_assert(offsetof(RTCHit, Ng_z) == offsetof(RTCHit, Ng_y) + 4, "Unexpected padding in RayBatch");

        // TODO: ensure that: eigen may store additional data _after_ the floats

        RTCRay &firstRay = embRayHits.get(0).ray;
        RTCHit &firstHit = embRayHits.get(0).hit;

        const size_t stride = sizeof(RTCRayHit);
        std::shared_ptr<MemoryKeeper> mem = embRayHits.memory();

        const Matrix<int, 1, 1> bufferSize    (size);
        const Matrix<int, 1, 1> bufferStrides1(static_cast<int>(stride)               );
        const Matrix<int, 2, 1> bufferStrides2(static_cast<int>(stride), sizeof(float));

        initBuffers(
            SBufferXu ::create <1>( mem, &firstRay.id,      bufferSize, bufferStrides1 ),
            SBufferX3f::create <1>( mem, &firstRay.org_x,   bufferSize, bufferStrides2 ),
            SBufferX3f::create <1>( mem, &firstRay.dir_x,   bufferSize, bufferStrides2 ),
            SBufferXf ::create <1>( mem, &firstRay.tfar,    bufferSize, bufferStrides1 ),
            SBufferXu ::create <1>( mem, &firstHit.geomID,  bufferSize, bufferStrides1 ),
            SBufferXu ::create <1>( mem, &firstHit.primID,  bufferSize, bufferStrides1 )
        );
    }

    void EmbreeBatch::clear() {
        if (cleared)
            return;

        for (int ii=0; ii<size(); ++ii) {
            RTCRay &ray = embRayHits.get(ii).ray;
            RTCHit &hit = embRayHits.get(ii).hit;

            hit.geomID = RTC_INVALID_GEOMETRY_ID;
            hit.instID[0] = RTC_INVALID_GEOMETRY_ID;
        }

        cleared = true;
    }

    SBuffer<RTCRayHit, DYN> EmbreeBatch::getEmbreeRayHits() {
        return embRayHits;
    }
}
